//题目分析:
//用了map,具体查看msdn或primer...
//由于数据庞大,建议用scanf,坑爹的scanf和cin的巨大时间差别

//题目网址:http://soj.me/4189

#include<iostream>
#include<cstdio>
#include<map>
using namespace std;
int main()
{
    int t, i;
    map<int,int> m;
    while(cin >> t && t != 0){
        while(t--){
            scanf("%d",&i);
            m[i] = i;
        }
        cout << m.size() << endl;
        m.clear();
    }
    return 0;
}                                 